#pragma once

#include "CImgStoredImage.h"
#include "ConsoleInput.h"
#include "Utilities.h"

class ImageSelfImpl final: public CImgStoredImage
{
	using CImgStoredImage::CImgStoredImage;

	public:

		void MosaicAlgorithm() override;

	private:

		[[nodiscard]] std::vector<cimg_library::CImg <uint8_t>> GetImageCells();
		void ReplaceCell(uint32_t cellNumber, const cimg_library::CImg <uint8_t>&);
};

