#pragma once

#include "OpenCVStoredImage.h"
#include "ConsoleInput.h"

class ImageOpenCVImpl final: public OpenCVStoredImage
{
	using OpenCVStoredImage::OpenCVStoredImage;

	public:

		void MosaicAlgorithm() override;

	private:

		[[nodiscard]] std::vector<cv::Mat> GetImageCells();
		void ReplaceCell(uint32_t cellNumber, cv::Mat&&);
};

