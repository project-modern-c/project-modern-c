#include "ConsoleInput.h"
#include "../Logger/Logger.h"

#include <iostream>
#include <optional>
#include <sstream>
#include <filesystem>

#include <regex>

ConsoleInput& ConsoleInput::GetInstance()
{
	static ConsoleInput object;
	return object;
}
	
auto findCommandString = [&](const std::map <std::string, std::string>& inputCommands, std::string_view longForm, std::string_view shortForm) -> std::optional<std::string>
{
	if (auto mapItr = inputCommands.find(std::string(longForm)); mapItr != inputCommands.end() && mapItr->second != "")
		return mapItr->second;

	if (auto mapItr = inputCommands.find(std::string(shortForm)); mapItr != inputCommands.end() && mapItr->second != "")
		return mapItr->second;

	return std::nullopt;
};

void PrintHelpCLI(std::map <std::string, std::string> inputCommands)
{
	if (inputCommands.find("--help") != inputCommands.end() || inputCommands.find("-h") != inputCommands.end())
	{
		std::cout << "\nCLI DOCUMENTATION:\n";

		std::cout << std::left << std::setw(20) << "ADD-TO-COLLECTION" << "Adds image to set of images designed for doing the mosaication\n";
		std::cout << std::left << std::setw(20) << "CELL-SIZE"  << "Sets size for cells of input image\n";
		std::cout << std::left << std::setw(20) << "HELP"  << "Prints functionality of every command of CLI\n";
		std::cout << std::left << std::setw(20) << "INPUT-FILE"  << "Sets absolute path for input image provided for mosaic algorithm\n";
		std::cout << std::left << std::setw(20) << "LIBRARY"  << "Sets library object type for input image\n";
		std::cout << std::left << std::setw(20) << "MOSAIC"  << "Generates mosaic photo of square form using input image and set of images\n";
		std::cout << std::left << std::setw(20) << "OUTPUT-PATH"  << "Sets absolute path for mosaicated output image\n";
		std::cout << std::left << std::setw(20) << "OUTPUT-TYPE"  << "Sets type of photo for mosaicated output image\n";
		std::cout << std::left << std::setw(20) << "BENCHMARK"  << "Benchmark the Mosaic functions using all three available libraries\n\n\n";
	}
}

ImplType GetLibraryFromCommands(const std::map <std::string, std::string>& inputCommands)
{
	ImplType library = kDefaultLibrary;

	if (auto libraryTemp = findCommandString(inputCommands, "--library", "-l"); libraryTemp.has_value())
	{
		auto libraryType = GetImplTypeFromString(libraryTemp.value());

		if (libraryType.has_value())
			library = libraryType.value();
		else
			Logger::GetInstance().Log(std::string_view("Invalid value for --library, using default"), Logger::LogLevel::WARNING);
	}
	else
	{
		Logger::GetInstance().Log(std::string_view("Using default library"), Logger::LogLevel::INFO);
	}

	return library;
}

std::vector<std::string> GetBaseImagesFromCommands(const std::map <std::string, std::string>& inputCommands)
{
	std::vector<std::string> imagesPath;

	if (auto BaseImages = findCommandString(inputCommands, "--input-file", "-i"); BaseImages.has_value())
	{
		std::string inputPaths{ BaseImages.value() };

		std::smatch regexMatch;
		std::regex imagePathExpression { "([^ ][^.]*(\.png|\.jpg|\.jpeg|\.bmp))", std::regex_constants::icase };

		while (std::regex_search(inputPaths, regexMatch, imagePathExpression))
		{
			imagesPath.push_back(regexMatch[0]);
			inputPaths = regexMatch.suffix();
		}

		if (imagesPath.size() == 0)
			Logger::GetInstance().Log(std::string_view("No valid input file provided"), Logger::LogLevel::ERROR);
	}
	else
	{
		Logger::GetInstance().Log(std::string_view("No input file provided"), Logger::LogLevel::ERROR);
	}

	return imagesPath;
}

uint16_t GetCellSizeFromCommands(const std::map <std::string, std::string>& inputCommands)
{
	uint16_t cellSize = kDefaultCellSize;

	if (auto cellSizeTemp = findCommandString(inputCommands, "--cell-size", "-c"); cellSizeTemp.has_value())
	{
		try
		{
			cellSize = std::stoi(cellSizeTemp.value());
		}
		catch (std::invalid_argument e)
		{
			Logger::GetInstance().Log(std::string_view("Invalid value for --cell-size, using default"), Logger::LogLevel::WARNING);
		}
	}
	else
	{
		Logger::GetInstance().Log(std::string_view("Using default cell size"), Logger::LogLevel::INFO);
	}

	return cellSize;
}

ImageType GetOutputTypeFromCommands(const std::map <std::string, std::string>& inputCommands)
{
	ImageType outputType = kDefaultOutputType;

	if (auto outputTypeTemp = findCommandString(inputCommands, "--extension", "-e"); outputTypeTemp.has_value())
	{
		auto imageType = GetImageTypeFromString(outputTypeTemp.value());

		if (imageType.has_value())
			outputType = imageType.value();
		else
			Logger::GetInstance().Log(std::string_view("Invalid value for --extension, using default"), Logger::LogLevel::WARNING);
	}
	else
	{
		Logger::GetInstance().Log(std::string_view("Using default output image extension"), Logger::LogLevel::INFO);
	}

	return outputType;
}

std::string GetOutputPathFromCommands(const std::map <std::string, std::string>& inputCommands)
{
	std::string outputPath = kDefaultOutputPath;

	if (auto outputPathTemp = findCommandString(inputCommands, "--output-path", "-o"); outputPathTemp.has_value())
	{
		if (std::filesystem::is_directory(outputPathTemp.value()))
		{
			outputPath = outputPathTemp.value();

			if (!outputPath.ends_with("\\"))
				outputPath.append("\\");
		}
		else
		{
			Logger::GetInstance().Log(std::string_view("Invalid value for --output-path"), Logger::LogLevel::WARNING);
		}
	}
	else
	{
		Logger::GetInstance().Log(std::string_view("Using default output path"), Logger::LogLevel::INFO);
	}

	return outputPath;
}

uint16_t GetBenchmarkTimesFromCommands(const std::map <std::string, std::string>& inputCommands)
{
	uint16_t benchTimes = kDefaultBenchmarkRuns;

	if (auto benchTimesTemp = findCommandString(inputCommands, "--benchmark-runs", "-c"); benchTimesTemp.has_value())
	{
		try
		{
			benchTimes = std::stoi(benchTimesTemp.value());
		}
		catch (std::invalid_argument e)
		{
			Logger::GetInstance().Log(std::string_view("Invalid value for --benchmark-runs, using default"), Logger::LogLevel::WARNING);
		}
	}
	else
	{
		Logger::GetInstance().Log(std::string_view("Using default benchmark runs"), Logger::LogLevel::INFO);
	}

	return benchTimes;
}

void ConsoleInput::Init(int argumentsCount, char** argument)
{

	if (argumentsCount == 1)
		throw std::invalid_argument("No arguments provided");

	std::map <std::string, std::string> inputCommands;

	for (auto iArg = 1u; iArg < argumentsCount; ++iArg) //incepem circul de la primul arg pentru ca pe indexul 0 se afla path-ul executabilului nostru
	{
		if (argument[iArg][0] == '-')
		{
			inputCommands.insert(std::make_pair(argument[iArg], ""));

			for (auto iOptionArg = iArg + 1; iOptionArg < argumentsCount; ++iOptionArg)
			{
				if (argument[iOptionArg][0] == '-')
				{
					{
						if (inputCommands[argument[iArg]].ends_with(" "))
							inputCommands[argument[iArg]].pop_back();

						iArg = iOptionArg - 1;
						break;
					}
				}

				inputCommands[argument[iArg]].append(argument[iOptionArg]);

				if (iOptionArg == argumentsCount - 1)
				{
					{
						if (inputCommands[argument[iArg]].ends_with(" "))
							inputCommands[argument[iArg]].pop_back();

						iArg = iOptionArg - 1;
						break;
					}
				}
				else 
					inputCommands[argument[iArg]].append(" ");
			}
		}
	}

	if (inputCommands.find("--mosaic") != inputCommands.end())
		m_shouldMosaic = true;

	if (inputCommands.find("--add-to-collection") != inputCommands.end())
		m_shouldAddToCollection = true;

	if (inputCommands.find("--benchmark") != inputCommands.end())
	{
		m_shouldBenchmark = true;
		m_benchmarkTimes = GetBenchmarkTimesFromCommands(inputCommands);
	}

	if (m_shouldBenchmark || m_shouldMosaic || m_shouldAddToCollection)
	{
		m_imagesPath = GetBaseImagesFromCommands(inputCommands);
		m_cellSize = GetCellSizeFromCommands(inputCommands);
		m_outputType = GetOutputTypeFromCommands(inputCommands);
		m_outputPath = GetOutputPathFromCommands(inputCommands);

		if(!m_shouldBenchmark)
			m_library = GetLibraryFromCommands(inputCommands);
	}
		
	PrintHelpCLI(inputCommands);
}

const std::vector<std::string> ConsoleInput::GetInputImagesPaths() const noexcept
{
	return m_imagesPath;
}

const uint16_t ConsoleInput::GetCellSize() const noexcept
{
	return m_cellSize;
}

const ImageType ConsoleInput::GetOutputImageType() const noexcept
{
	return m_outputType;
}

const ImplType ConsoleInput::GetLibrary() const noexcept
{
	return m_library;
}

const std::string ConsoleInput::GetOutputPath() const noexcept
{
	return m_outputPath;
}

const uint16_t ConsoleInput::GetBenchmarkTimes()
{
	return m_benchmarkTimes;
}

const bool ConsoleInput::ShouldMosaic() const noexcept
{
	return m_shouldMosaic;
}

const bool ConsoleInput::ShouldAddToCollection() const noexcept
{
	return m_shouldAddToCollection;
}

const bool ConsoleInput::ShouldBenchmark() const noexcept
{
	return m_shouldBenchmark;
}

