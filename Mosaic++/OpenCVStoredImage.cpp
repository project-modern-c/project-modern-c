#include "OpenCVStoredImage.h"

OpenCVStoredImage::OpenCVStoredImage(const std::string& inputImagePath)
	: BaseImage(inputImagePath), m_Image {}
{
	m_Image = cv::imread(inputImagePath, cv::IMREAD_UNCHANGED);

	if(m_Image.empty())
		throw std::invalid_argument("Can't load image " + inputImagePath);
}

[[nodiscard]] const cv::Mat& OpenCVStoredImage::GetRawImageObject() const
{
	return m_Image;
}

void OpenCVStoredImage::Save(const std::string& outputPath, const std::string& fileName, ImageType extension)
{
	const auto imageSaveFullPath = std::string(outputPath + fileName + GetStringFromImageType(extension));
	
	Logger::GetInstance().Log("Image saved in " + imageSaveFullPath, Logger::LogLevel::INFO);

	cv::imwrite(imageSaveFullPath, m_Image);
}
