#pragma once

#include <cstdint>
#include <optional>

#include <iostream>
#include <algorithm>
#include <optional>

enum class ImageType : uint8_t
{
	JPEG,
	PNG,
	BMP,
	JPG
};

inline std::optional<ImageType> GetImageTypeFromString(std::string& extensionString) noexcept
{
	std::transform(extensionString.begin(), extensionString.end(), extensionString.begin(),
		[](unsigned char c) { return std::tolower(c); });

	if (extensionString == ".png")
		return ImageType::PNG;
	else if (extensionString == ".jpeg")
		return ImageType::JPEG;
	else if (extensionString == ".bmp")
		return ImageType::BMP;
	else if (extensionString == ".jpg")
		return ImageType::JPG;

	return std::nullopt;
}

inline const std::string GetStringFromImageType(ImageType imageType)
{
	switch (imageType)
	{
		case ImageType::JPEG:
			return ".jpeg";		
		case ImageType::JPG:
			return ".jpg";
		case ImageType::PNG:
			return ".png";		
		case ImageType::BMP:
			return ".bmp";
		default:
			throw std::invalid_argument("Invalid image type");
	}
}