#include <iostream>

#include <algorithm>
#include <cstdint>
#include <string>
#include <regex>

#include "ImageType.h"
#include "CImg.h"
#include "ImagesContainer.h"
#include "ConsoleInput.h"
#include "Utilities.h"

#include "ImageSelfImpl.h"
#include "ImageCImgImpl.h"
#include "ImageOpenCVImpl.h"

#include "Benchmark.h"

int main(int argc, char** argv)
{
	Logger::GetInstance().SetOutputStream(std::cout), Logger::GetInstance().SetLogLevel(Logger::LogLevel::INFO);

	auto& consoleInput = ConsoleInput::GetInstance();

	cimg_library::cimg::exception_mode() = 0; //quiet, by default se afiseaza un popup pe ecran

	try
	{
		consoleInput.Init(argc, argv);
	}
	catch (std::invalid_argument e)
	{
		Logger::GetInstance().Log(e.what(), Logger::LogLevel::ERROR);
		std::cout << "USAGE: CLI [--library] [--l]\n[<--input-file> [<arg1 arg2 .. argn>]] [<-i> [<arg1 arg2 .. argn>]]\n[<--cell-size> [<arg>]] [<-c> [<arg>]] [<--extension> [<arg>]] [<-e> [<arg>]]\n[<--output-path> [<arg>]] [<-o> [<arg>]]\n[--mosaic] [--add-to-collection] [--help]";
	}

	/*
		Algoritmul de mozaicare
	*/

	const auto &library = consoleInput.GetLibrary();

	if (consoleInput.ShouldMosaic())
	{
		const auto& outputType = GetStringFromImageType(consoleInput.GetOutputImageType());

		if (auto const inputImages = consoleInput.GetInputImagesPaths(); inputImages.size() > 0u)
		{
			switch (library)
			{
				case ImplType::Self: 
				{
					/*
						Mozaicare pentru implementarile proprii.
					*/

					for (auto imagePath : inputImages)
					{
						Logger::GetInstance().Log("Generating mosaic using self implementations. " + imagePath, Logger::LogLevel::INFO);

						ImageSelfImpl image { imagePath };
						image.MosaicAlgorithm();
						image.Save(consoleInput.GetOutputPath(), "mosaic_self_" + image.GetFileName(), consoleInput.GetOutputImageType());
					}

					break;
				}
				case ImplType::CImg:
				{
					/*
						Mozaicare pentru implementarile din CImg.
					*/

					for (auto imagePath : inputImages)
					{
						Logger::GetInstance().Log("Generating mosaic using CImg implementations. " + imagePath, Logger::LogLevel::INFO);

						ImageCImgImpl image { imagePath };
						image.MosaicAlgorithm();
						image.Save(consoleInput.GetOutputPath(), "mosaic_cimg_" + image.GetFileName(), consoleInput.GetOutputImageType());
					}

					break;
				}

				case ImplType::OpenCV:
				{
					/*
						Mozaicare pentru implementarile din OpenCV.
					*/

					for (auto imagePath : inputImages)
					{
						Logger::GetInstance().Log("Generating mosaic using OpenCV implementations. " + imagePath, Logger::LogLevel::INFO);

						ImageOpenCVImpl image { imagePath };
						image.MosaicAlgorithm();
						image.Save(consoleInput.GetOutputPath(), "mosaic_opencv_" + image.GetFileName(), consoleInput.GetOutputImageType());
					}

					break;
				}

			}
		}
	}

	/*
		Adaugare imagine la colectia de imagini
	*/

	if (consoleInput.ShouldAddToCollection())
	{
		if (auto const inputImages = consoleInput.GetInputImagesPaths(); inputImages.size() > 0u)
		{
			for (auto imagePath : inputImages)
			{
				Logger::GetInstance().Log("Adding default image " + imagePath + " to collection.", Logger::LogLevel::INFO);
				
				ImagesContainer::GetInstance().AddImageToCollection(imagePath, library);
			}
		}
	}

	if (consoleInput.ShouldBenchmark())
	{
		if (auto const inputImages = consoleInput.GetInputImagesPaths(); inputImages.size() > 0u)
		{
			/*
				Trebuie sa avem imagini de input pentru a face benchmark
			*/

			const auto iBenchTimes = consoleInput.GetBenchmarkTimes();

			/*
				Mosaic Benchmark
			*/

			for (auto imagePath : inputImages)
			{
				std::array<uint32_t, 3> runTimes;

				runTimes[0] = Benchmark::run_n_times("MosaicAlgorithm - Self", iBenchTimes, [&]() {
					ImageSelfImpl image { imagePath };
					image.MosaicAlgorithm();
					image.Save(consoleInput.GetOutputPath(), "mosaic_bench_self_" + image.GetFileName(), consoleInput.GetOutputImageType());
				});
				
				runTimes[1] = Benchmark::run_n_times("MosaicAlgorithm - OpenCV", iBenchTimes, [&]() {
					ImageOpenCVImpl image { imagePath };
					image.MosaicAlgorithm();
					image.Save(consoleInput.GetOutputPath(), "mosaic_bench_opencv_" + image.GetFileName(), consoleInput.GetOutputImageType());
				});

				runTimes[2] = Benchmark::run_n_times("MosaicAlgorithm - CImg", iBenchTimes, [&] {
					ImageCImgImpl image { imagePath };
					image.MosaicAlgorithm();
					image.Save(consoleInput.GetOutputPath(), "mosaic_bench_cimg_" + image.GetFileName(), consoleInput.GetOutputImageType());
				});

				std::cout << "Self run time (avg) " << runTimes[0] << "ms | OpenCv run time (avg) " << runTimes[1] << "ms | CImg run time (avg) " << runTimes[2] << "ms\n";
			}
		}
	}

	return 0;
}
