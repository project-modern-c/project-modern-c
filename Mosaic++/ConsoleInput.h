#pragma once

#include <map>
#include <string>
#include <vector>
#include "ImageType.h"
#include "ImplType.h"

/*
	Vector cu imagini de input (--input-file a.png b.png ... sau -i a.png b.png ...)
		

	Parametrii configurabili:
		Locatia imaginilor mozaic rezultate (--output-path cale.. sau -o cale)
		Rezolutia celulei (--cell-size rezolutie -c rezolutie (REZOLUTIA VA FI UN NUMAR))
		Extensia imaginii(lor) mozaic rezultate (--extension extensie (UNA DINTRE png, jpg, bmp) -e )
*/

const uint16_t kDefaultCellSize = 16u;
const ImageType kDefaultOutputType = ImageType::PNG;
const std::string kDefaultOutputPath = ""; //folderul cu programul
const ImplType kDefaultLibrary = ImplType::Self;
const uint16_t kDefaultBenchmarkRuns = 5u;

class ConsoleInput final
{
	private:
		ConsoleInput() = default;

	public:
		ConsoleInput(ConsoleInput const&) = delete;
		ConsoleInput& operator = (ConsoleInput const&) = delete;
		
		static ConsoleInput& GetInstance();
		void Init(int argumentsCount, char** argument);

		const std::vector<std::string> GetInputImagesPaths() const noexcept;
		const uint16_t GetCellSize() const noexcept;
		const ImageType GetOutputImageType() const noexcept;
		const ImplType GetLibrary() const noexcept;
		const std::string GetOutputPath() const noexcept;
		const uint16_t GetBenchmarkTimes();

		const bool ShouldMosaic() const noexcept;
		const bool ShouldAddToCollection() const noexcept;
		const bool ShouldBenchmark() const noexcept;

	private:

		/*
			m_imagesPath - Vectorul cu path-urile catre imaginile de input, camp pt dimensiunea celulei, e.t.c
			m_cellSize - Dimensiunea (Rezolutia) celulelor imaginii de input
			m_outputType - extensia imaginilor de output
		*/

		std::vector<std::string> m_imagesPath;
		uint16_t m_cellSize;
		ImageType m_outputType;
		ImplType m_library;
		std::string m_outputPath;
		bool m_shouldMosaic;
		bool m_shouldAddToCollection;
		bool m_shouldBenchmark;
		uint16_t m_benchmarkTimes;
		
};

