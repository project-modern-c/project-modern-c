#pragma once

#include <cstdint>
#include <string>
#include <vector>

#include "ImageType.h"
#include "ImagesContainer.h"

class BaseImage
{
	public:

		BaseImage (const std::string&);

		[[nodiscard]] const std::string& GetFileName() const noexcept;
		[[nodiscard]] const std::string& GetFileExtension() const noexcept;

		virtual void MosaicAlgorithm() = 0; //Overloaded in Impl
		virtual void Save(const std::string&, const std::string&, ImageType) = 0; //Overloaded in Store

	private:

		std::string m_fileName;
		std::string m_fileExtension;
};

