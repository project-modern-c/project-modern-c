#pragma once

#include <array>
#include <cstdint>
#include <string>
#include <numeric>
#include <memory>

#include "CImg.h"

#include <opencv2/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/highgui.hpp>

namespace utilities 
{
	cimg_library::CImg <uint8_t> GetCroppedImage(const cimg_library::CImg <uint8_t>&, const std::pair<uint16_t, uint16_t>&, const uint16_t, const uint16_t) noexcept;
	cimg_library::CImg <uint8_t> GetScaledImage(cimg_library::CImg <uint8_t>& image, uint32_t, uint32_t) noexcept;

	uint32_t GetMedianColor(const cimg_library::CImg <uint8_t>&) noexcept;
	uint32_t GetMedianColor(const cv::Mat&) noexcept;
}