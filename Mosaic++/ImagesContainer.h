#pragma once

#include <string>
#include <map>
#include <unordered_map>
#include <cstdint>
#include <filesystem>
#include <fstream>

#include "ImplType.h"

const std::string kFileName = "images.txt";

class ImagesContainer
{
	protected:
		ImagesContainer();
		~ImagesContainer();

	public:
		ImagesContainer(ImagesContainer const&) = delete;
		ImagesContainer& operator = (ImagesContainer const&) = delete;

		static ImagesContainer& GetInstance();

		const std::string GetImageByColorAndSize(uint32_t, uint32_t, ImplType);

		void AddImageToCollection(std::string&, ImplType);

	private:
		void AddPathToFileAndMap(std::string&, uint32_t, uint32_t);

	private:

		std::map<uint32_t, std::unordered_map<uint32_t, std::string>> m_paths;
		std::fstream m_storageFile;
	
};

