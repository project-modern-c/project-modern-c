#pragma once

#include "BaseImage.h"
#include "CImg.h"
#include "../Logger/Logger.h"

class CImgStoredImage : public BaseImage
{
	public:
		CImgStoredImage(const std::string&);

		[[nodiscard]] const cimg_library::CImg<uint8_t>& GetRawImageObject() const;
		void Save(const std::string& outputPath, const std::string& fileName, ImageType extension) override;
		
	protected:
		cimg_library::CImg<uint8_t> m_Image;
};

