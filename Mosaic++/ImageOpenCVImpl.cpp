#include "ImageOpenCVImpl.h"
#include "Utilities.h"

void ImageOpenCVImpl::MosaicAlgorithm()
{
	const auto iCellSize = ConsoleInput::GetInstance().GetCellSize();

	const auto &imageCellsVector = GetImageCells();
	const auto iCellsCount = imageCellsVector.size();
	auto& imagesContainer = ImagesContainer::GetInstance();

	std::stringstream strStream;

	strStream << "Image " << GetFileName() << " has " << iCellsCount << " cells";

	Logger::GetInstance().Log(strStream.str(), Logger::LogLevel::INFO);
		
	for (auto cellImage = 0u; cellImage < iCellsCount; ++cellImage)
	{
		/*
			Se calculeaza culoarea medie pentru celula imagine.
			Se alege din colectia imaginea potrivita pentru a inlocui celula (prin imagine potrivita se intelege o imagine cu culoarea medie cea mai apropiata de cea a celulei).
		*/

		uint32_t cellMedianColor = utilities::GetMedianColor(imageCellsVector[cellImage]);
		std::string pathImageReplacement = imagesContainer.GetImageByColorAndSize(cellMedianColor, iCellSize, ImplType::OpenCV);

		cv::Mat cellImageReplacement = cv::imread(pathImageReplacement, cv::IMREAD_UNCHANGED);
		
		ReplaceCell(cellImage, std::move(cellImageReplacement));
	}

	Logger::GetInstance().Log("Cells replaced for image " + GetFileName(), Logger::LogLevel::INFO);
}

[[nodiscard]] std::vector<cv::Mat> ImageOpenCVImpl::GetImageCells()
{
	/*
		In cazul in care impartirea in celule nu poate fi facuta exact, vom cropa putin imaginea pentru a face acest lucru posibil
	*/

	const auto iCellSize = ConsoleInput::GetInstance().GetCellSize();

	const auto heightDifference = m_Image.size[0] % iCellSize;
	const auto widthDifference = m_Image.size[1] % iCellSize;

	if (heightDifference != 0)
		m_Image = m_Image(cv::Rect(0u, heightDifference / 2, m_Image.size[1], m_Image.size[0] - heightDifference / 2));

	if (widthDifference != 0)
		m_Image = m_Image(cv::Rect(widthDifference / 2, 0u, m_Image.size[1] - widthDifference / 2, m_Image.size[0]));

	const auto widthCount = m_Image.size[1] / iCellSize;
	const auto heightCount = m_Image.size[0] / iCellSize;

    std::vector<cv::Mat> imageCells;

	for (auto i = 0u; i < widthCount; ++i)
		for (auto j = 0u; j < heightCount; ++j)
			imageCells.emplace_back((m_Image(cv::Rect(i * iCellSize, j * iCellSize, iCellSize, iCellSize))));

	return imageCells;
}

void ImageOpenCVImpl::ReplaceCell(uint32_t cellNumber, cv::Mat&& image)
{
	const auto iCellSize = ConsoleInput::GetInstance().GetCellSize();

	const int cellsHeight = m_Image.size[0] / iCellSize;
	const int cellsWidth = m_Image.size[1] / iCellSize;

	if (cellNumber >= cellsHeight * cellsWidth)
		throw std::invalid_argument("Invalid cell number.");
	
	auto subImage = m_Image(cv::Rect(cellNumber / cellsHeight * iCellSize, cellNumber % cellsHeight * iCellSize, iCellSize, iCellSize));

	if (image.channels() == 1)
		cv::cvtColor(subImage, subImage, cv::COLOR_BGR2GRAY); // Now crop is grayscale CV_8UC1

	image.copyTo(subImage);
}
