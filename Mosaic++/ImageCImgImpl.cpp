#include "ImageCImgImpl.h"

void ImageCImgImpl::MosaicAlgorithm()
{
	const auto iCellSize = ConsoleInput::GetInstance().GetCellSize();

	const auto& imageCellsVector = GetImageCells();
	const auto iCellsCount = imageCellsVector.size();

	std::stringstream strStream;

	strStream << "Image " << GetFileName() << " has " << iCellsCount << " cells";
	
	Logger::GetInstance().Log(strStream.str(), Logger::LogLevel::INFO);

	auto& imagesContainer = ImagesContainer::GetInstance();

	for (auto cellImage = 0u; cellImage < iCellsCount; ++cellImage)
	{
		/*
			Se calculeaza culoarea medie pentru celula imagine
			Se alege din colectia imaginea potrivita pentru a inlocui celula (prin imagine potrivita se intelege o imagine cu culoarea medie cea mai apropiata de cea a celulei)
		*/

		uint32_t cellMedianColor = utilities::GetMedianColor(imageCellsVector[cellImage]);
		std::string pathImageReplacement = imagesContainer.GetImageByColorAndSize(cellMedianColor, iCellSize, ImplType::CImg);

		cimg_library::CImg<uint8_t> cellImageReplacement { pathImageReplacement.c_str() };
		ReplaceCell(cellImage, cellImageReplacement);
	}

	Logger::GetInstance().Log("Cells replaced for image " + GetFileName(), Logger::LogLevel::INFO);
}

[[nodiscard]] std::vector<cimg_library::CImg <uint8_t>> ImageCImgImpl::GetImageCells()
{
	const auto iCellSize = ConsoleInput::GetInstance().GetCellSize();

	if (iCellSize > m_Image.height() || iCellSize < 1)
		throw std::runtime_error("Invalid cell size");

	/*
		In cazul in care impartirea in celule nu poate fi facuta exact, vom cropa putin imaginea pentru a face acest lucru posibil
	*/

	const auto heightDifference = m_Image.height() % iCellSize;
	const auto widthDifference = m_Image.width() % iCellSize;

	if (heightDifference != 0)
		m_Image.crop(0u, heightDifference / 2, m_Image.width(), m_Image.height() - heightDifference / 2);

	if (widthDifference != 0)
		m_Image.crop(widthDifference / 2, 0u, m_Image.width() - widthDifference / 2, m_Image.height());

	const auto widthCount = m_Image.width() / iCellSize;
	const auto heightCount = m_Image.height() / iCellSize;

	std::vector<cimg_library::CImg <uint8_t>> imageCells;

	/*
		Functia get_crop din CImg obtine o subimagine dintr-o imagine.
	*/

	for (auto i = 0u; i < widthCount; ++i)
		for (auto j = 0u; j < heightCount; ++j)
			imageCells.emplace_back(m_Image.get_crop(i * iCellSize, j * iCellSize, (i + 1) * iCellSize, (j + 1) * iCellSize));

	return imageCells;
}

void ImageCImgImpl::ReplaceCell(uint32_t cellNumber, const cimg_library::CImg <uint8_t>& image)
{
	const auto iCellSize = ConsoleInput::GetInstance().GetCellSize();

	const int cellsHeight = m_Image.height() / iCellSize;
	const int cellsWidth = m_Image.width() / iCellSize;

	if (cellNumber >= cellsHeight * cellsWidth)
		throw std::invalid_argument("Invalid cell number.");

	/*
		Functia draw_image din CImg deseneaza o imagine peste o portiune a altei imagini (exact ce face functia ReplaceCell in implementarea proprie)
	*/

	m_Image.draw_image(cellNumber / cellsHeight * iCellSize, cellNumber % cellsHeight * iCellSize, image, 100);
}
