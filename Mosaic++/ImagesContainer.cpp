#include "ImagesContainer.h"
#include "Utilities.h"

#include "CImg.h"

#include "ImageSelfImpl.h"
#include "ImageCImgImpl.h"
#include "ImageOpenCVImpl.h"

#include <filesystem>
#include <unordered_set>

ImagesContainer::ImagesContainer()
    : m_storageFile { kFileName, std::ios::in | std::ios::out | std::ios::app }
{
    if (m_storageFile.bad())
        throw std::runtime_error("Failed to open images file." + kFileName);

    uint32_t medianColor;
    uint32_t size;
    std::string path;

    /*
        In caz ca mai gaseste alte poze cu aceeasi culoare medie, mai adauga in cadrul aceleiasi chei poza noua citita din fisier cu dimensiunile sale
    */

    std::unordered_set<std::string> imagesToDelete;

    while (m_storageFile >> medianColor >> size >> path) 
    {
        if (!std::filesystem::is_regular_file(path))
        {
            Logger::GetInstance().Log(std::string_view{ "Invalid file " + path + ", marked for deletion " + kFileName }, Logger::LogLevel::ERROR);
            
            imagesToDelete.insert(path);
            continue;
        }

        auto iterator = m_paths.find(medianColor);

        if (iterator != m_paths.end()) 
            iterator->second.insert(std::make_pair(size,path));
        else
            m_paths[medianColor] = std::move(std::unordered_map<uint32_t, std::string>{std::make_pair(size, path)});
    }


    /*
        Are loc o refacere a fisierului images.txt, stergandu'se imaginile care nu mai exista pe disk.
        N-am gasit o solutie 'eleganta' sa stergem o linie dintr-un fisier fara a il reface.
    */

    if (imagesToDelete.size())
    {
        m_storageFile.close();

        std::filesystem::rename(kFileName, "old_" + kFileName); 

        m_storageFile = std::fstream { kFileName, std::ios::in | std::ios::out | std::ios::app }; 
        
        auto m_readFile = std::ifstream { "old_" + kFileName }; 

        while (m_readFile >> medianColor >> size >> path)
            if (imagesToDelete.find(path) == imagesToDelete.end())
                m_storageFile << medianColor << " " << size << " " << path << '\n';

        m_readFile.close();
        std::filesystem::remove("old_" + kFileName);
    }
}

ImagesContainer::~ImagesContainer()
{
    m_storageFile.close();
}

ImagesContainer& ImagesContainer::GetInstance()
{
    static ImagesContainer object;
    return object;
}

const std::string ImagesContainer::GetImageByColorAndSize(uint32_t medianColor, uint32_t size, ImplType implType)
{
    auto thisColorItr = m_paths.find(medianColor);

    if (thisColorItr == m_paths.end()) 
    {
        /*
            Daca nu gasim exact culoarea medie dorita, cautam o poza cu cea mai apropiata culoare medie de ea
        */

        uint32_t closestColor;
        int closestColorDif = INT_MAX;

        for (auto& itr : m_paths)
        {
            auto closestDif = std::abs(static_cast<int>(itr.first - medianColor));

            if (closestDif > closestColorDif)
                break;

            if (closestDif < closestColorDif)
            {
                closestColor = itr.first;
                closestColorDif = closestDif;
            }
        }

        thisColorItr = m_paths.find(closestColor);
        medianColor = closestColor;
    }

    const auto imageWithSize = thisColorItr->second.find(size);

    if (imageWithSize != thisColorItr->second.end())
        return imageWithSize->second;

    /*
        Daca nu gasim deja in map o imagine cu rezolutia dorita, redimensionam o imagine la aceasta rezolutie si o adaugam in map
    */

    /* 
        O imagine random din cele cu culoarea medie selectata anterior 
    */

    std::string imageWithColorPath = thisColorItr->second.begin()->second;

    /*
        Crearea unui nou path pentru noua imagine
    */

    const auto iExtension = imageWithColorPath.find_last_of(".");
    std::string newImagePath = imageWithColorPath.substr(0, iExtension) + "_res_" + std::to_string(size) + imageWithColorPath.substr(iExtension);

    /*
        Salvam in folder imaginea nou creata
        Avand in vedere ca este o singura operatie de facut ce necesita implementare 'proprie' (scalarea), pentru a elimina din codul duplicat se va face un switch 
    */

    switch (implType)
    {
        case ImplType::Self: 
        {
            auto oldImage = cimg_library::CImg <uint8_t> { imageWithColorPath.c_str() };
            auto newImage = utilities::GetScaledImage(oldImage, size, size);

            newImage.save(newImagePath.c_str());
            break;
        }
        case ImplType::CImg:
        {
            cimg_library::CImg <uint8_t> newImage { imageWithColorPath.c_str() };

            newImage.resize(size, size);
            newImage.save(newImagePath.c_str());
            break;
        }
        case ImplType::OpenCV:
        {
            auto oldImage = cv::imread(imageWithColorPath, cv::IMREAD_ANYCOLOR);
            auto newImage = cv::Mat { };

            cv::resize(oldImage, newImage, cv::Size(size, size), 0.0, 0.0, cv::INTER_NEAREST);
            cv::imwrite(newImagePath, newImage);
            break;
        }
    }

    AddPathToFileAndMap(newImagePath, medianColor, size);
    return newImagePath;
}

void ImagesContainer::AddImageToCollection(std::string& imagePath, ImplType implType)
{

    std::string newPath { "../Mosaic/Images/Manual_Added/" };
    std::string newPathResized { "../Mosaic/Images/Manual_Added/" };

    uint32_t iMedianColor;
    uint32_t iImageSize;

    switch (implType)
    {
        case ImplType::Self:
        {
            ImageSelfImpl diskImage { imagePath.c_str() };

            /*
                Salvare imagine asa cum este
            */

            auto rawImage = diskImage.GetRawImageObject();

            iImageSize = std::min(rawImage.width(), rawImage.height());
            rawImage = utilities::GetScaledImage(rawImage, iImageSize, iImageSize);
            iMedianColor = utilities::GetMedianColor(rawImage);

            newPath += diskImage.GetFileName() + diskImage.GetFileExtension();
            rawImage.save(newPath.c_str());

            /*
                Preprocesare imagine la dimensiunea default a celulei
            */

            auto resizedImage = utilities::GetScaledImage(rawImage, kDefaultCellSize, kDefaultCellSize);

            newPathResized += diskImage.GetFileName() + "_res_" + std::to_string(kDefaultCellSize) + diskImage.GetFileExtension();
            resizedImage.save(imagePath.c_str());

            break;
        }
        case ImplType::CImg:
        {

            ImageCImgImpl diskImage { imagePath.c_str() };

            /*
                Salvare imagine asa cum este
            */

            auto rawImage = diskImage.GetRawImageObject();

            iImageSize = std::min(rawImage.width(), rawImage.height());
            rawImage.resize(iImageSize, iImageSize);
            iMedianColor = utilities::GetMedianColor(rawImage);

            newPath += diskImage.GetFileName() + diskImage.GetFileExtension();
            rawImage.save(newPath.c_str());

            /*
                Preprocesare imagine la dimensiunea default a celulei
            */

            rawImage.resize(kDefaultCellSize, kDefaultCellSize);

            newPathResized += diskImage.GetFileName() + "_res_" + std::to_string(kDefaultCellSize) + diskImage.GetFileExtension();
            rawImage.save(imagePath.c_str());

            break;
        }
        case ImplType::OpenCV:
        {

            ImageOpenCVImpl diskImage { imagePath.c_str() };

            /*
                Salvare imagine asa cum este
            */

            auto rawImage = diskImage.GetRawImageObject();

            iImageSize = std::min(rawImage.size[1], rawImage.size[0]);

            cv::resize(rawImage, rawImage, cv::Size(iImageSize, iImageSize), 0.0, 0.0, cv::INTER_NEAREST);
            iMedianColor = utilities::GetMedianColor(rawImage);

            newPath += diskImage.GetFileName() + diskImage.GetFileExtension();
            cv::imwrite(newPath.c_str(), rawImage);

            /*
                Preprocesare imagine la dimensiunea default a celulei
            */

            cv::resize(rawImage, rawImage, cv::Size(kDefaultCellSize, kDefaultCellSize), 0.0, 0.0, cv::INTER_NEAREST);

            newPath += diskImage.GetFileName() + diskImage.GetFileExtension();
            cv::imwrite(newPath.c_str(), rawImage);

            break;
        }
    }

    AddPathToFileAndMap(newPath, iMedianColor, iImageSize);
    AddPathToFileAndMap(newPathResized, iMedianColor, kDefaultCellSize);
}

void ImagesContainer::AddPathToFileAndMap(std::string& imagePath, uint32_t iMedianColor, uint32_t iImageSize)
{
    /*
        Salvare in fisier.
    */

    m_storageFile.clear();
    m_storageFile << iMedianColor << " " << iImageSize << " " << imagePath << '\n';

    /*
        Se adauga path-ul in map pentru o posibila iteratie viitoare'
    */

    m_paths.find(iMedianColor)->second.insert(std::make_pair(iImageSize, imagePath));
}