#pragma once

#include <string>
#include <optional>
#include <algorithm>

enum class ImplType : uint8_t
{
	Self,
	OpenCV,
	CImg,
};

inline std::optional<ImplType> GetImplTypeFromString(std::string& implString) noexcept
{
	std::transform(implString.begin(), implString.end(), implString.begin(),
		[](unsigned char c) { return std::tolower(c); });

	if (implString == "self")
		return ImplType::Self;

	else if (implString == "opencv")
		return ImplType::OpenCV;

	else if (implString == "cimg")
		return ImplType::CImg;

	return std::nullopt;
}