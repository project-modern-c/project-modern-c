#pragma once

#include <string>
#include <functional>
#include <chrono>

struct Benchmark
{
    template <typename F, typename ...Args>

    static void run (const std::string& funcName, F func, Args&&... args)
    {
        auto start = std::chrono::system_clock::now();

        func(std::forward<Args>(args)...);

        auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now() - start);

        std::stringstream outStream;
        outStream << "1 run of " << funcName << " " << " took " << duration.count() << " ms \n";
        
        Logger::GetInstance().Log(outStream.str(), Logger::LogLevel::WARNING);
    }

    template <typename F, typename ...Args>

    static uint32_t run_n_times(const std::string& funcName, uint32_t times, F func, Args&&... args)
    {
        std::vector<uint32_t> executionTimes; executionTimes.reserve(times);

        for (auto run = 0u; run < times; ++run)
        {
            auto start = std::chrono::system_clock::now();

            func(std::forward<Args>(args)...);

            executionTimes.emplace_back(std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now() - start).count());
        }       

        auto averageRuns = (std::accumulate(executionTimes.begin(), executionTimes.end(), 0) / times);

        std::stringstream outStream;

        outStream << times << " runs of " << funcName 
            << " took " << std::accumulate(executionTimes.begin(), executionTimes.end(), 0) 
            << " ms with average " << averageRuns << "ms, " 
            << *std::max_element(executionTimes.begin(), executionTimes.end()) << "ms in worst case and " 
            << *std::min_element(executionTimes.begin(), executionTimes.end()) << "ms in best case \n";
        
        Logger::GetInstance().Log(outStream.str(), Logger::LogLevel::WARNING);
        
        return averageRuns;
    }
};