#include "Utilities.h"

uint32_t utilities::GetMedianColor (const cimg_library::CImg <uint8_t>& image) noexcept
{
    /*
        Implementare culoare medie proprie, avand drept imagine de baza o imagine CImg (n-am gasit o functie echivalenta in CImg)
    */

    auto iImageChannels = std::clamp(image.spectrum(), 1, 3);
    auto const iPixelsCount = image.width() * image.height();

    std::vector<uint32_t> pixelsMean; 
    pixelsMean.resize(iImageChannels);

    for (auto channelId = 0u; channelId < iImageChannels; ++channelId)
        pixelsMean[channelId] = std::accumulate(image.begin() + iPixelsCount * channelId, image.begin() + iPixelsCount * (channelId + 1), 0) / iPixelsCount;
    
    if (iImageChannels == 1)
        return pixelsMean[0] << 16 | pixelsMean[0] << 8 | pixelsMean[0];

    return pixelsMean[0] << 16 | pixelsMean[1] << 8 | pixelsMean[2];
}

uint32_t utilities::GetMedianColor(const cv::Mat& image) noexcept
{
    /*
        Implementare culoare medie folosind OpenCv.
    */

    const auto openCVMeanColor = cv::mean(image);

    if (image.channels() == 1)
    {
        uint8_t channelAvg = static_cast<uint8_t>(openCVMeanColor.val[0]);
        return channelAvg << 16 | channelAvg << 8 | channelAvg;
    }

    return static_cast<uint8_t>(openCVMeanColor.val[2]) << 16 | static_cast<uint8_t>(openCVMeanColor.val[1]) << 8 | static_cast<uint8_t>(openCVMeanColor.val[0]);
}

constexpr float LinearInterpolation(float colorNeighbourPixel1, float colorNeighbourPixel2, float iPixel) noexcept
{
    return colorNeighbourPixel1 + (colorNeighbourPixel2 - colorNeighbourPixel1) * iPixel;
}

constexpr float BilinearInterpolation(float c00, float c10, float c01, float c11, float xInterpolationPixel, float yInterpolationPixel) noexcept
{
    return LinearInterpolation(LinearInterpolation(c00, c10, xInterpolationPixel), LinearInterpolation(c01, c11, xInterpolationPixel), yInterpolationPixel);
}

cimg_library::CImg<uint8_t> utilities::GetScaledImage(cimg_library::CImg<uint8_t>& image, uint32_t iHeight, uint32_t iWidth) noexcept
{
    auto iImageChannels = static_cast<unsigned int>(std::clamp(image.spectrum(), 1, 3));

    cimg_library::CImg<uint8_t> newImage { iHeight, iWidth, 1, iImageChannels };

    for (auto widthPixel = 0; widthPixel < iWidth; ++widthPixel)
    {
        for (auto heightPixel = 0; heightPixel < iHeight; ++heightPixel)
        {
            /*
                Pixelul din imagine de unde luam culoarea 
            */

            float widthPixelImageFloat = static_cast<float>(widthPixel) / newImage.width() * (image.width() - 1);
            float heightPixelImageFloat = static_cast<float>(heightPixel) / newImage.height() * (image.height() - 1);

            int widthPixelImageInt = static_cast<int>(widthPixelImageFloat);  
            int heightPixelImageInt = static_cast<int>(heightPixelImageFloat);

            /*
                Pixeli vecini din imaginea originala a caror rgb il contopim pentru a obtine culoare pentru pixeli goli din newimage
            */

            std::array<std::array<uint16_t, 3>, 4> neighbourPixel;

            for (auto channelId = 0u; channelId < iImageChannels; ++channelId)
            {
                neighbourPixel[0][channelId] = image(widthPixelImageInt, heightPixelImageInt, channelId);
                neighbourPixel[1][channelId] = image(widthPixelImageInt + 1, heightPixelImageInt, channelId);
                neighbourPixel[2][channelId] = image(widthPixelImageInt, heightPixelImageInt + 1, channelId);
                neighbourPixel[3][channelId] = image(widthPixelImageInt + 1, heightPixelImageInt + 1, channelId);

                newImage(widthPixel, heightPixel, 0, channelId) = static_cast<uint32_t>(BilinearInterpolation(neighbourPixel[0][channelId], neighbourPixel[1][channelId], neighbourPixel[2][channelId], neighbourPixel[3][channelId], widthPixelImageFloat - widthPixelImageInt, heightPixelImageFloat - heightPixelImageInt));  
            }
        }
    }

    return newImage;
}

cimg_library::CImg <uint8_t> utilities::GetCroppedImage(const cimg_library::CImg <uint8_t>& image, const std::pair<uint16_t, uint16_t>& startPoint, const uint16_t imageWidth, const uint16_t imageHeight) noexcept
{
    auto iImageChannels = std::clamp(image.spectrum(), 1, 3);

    const std::pair<uint16_t, uint16_t> endPoint = { startPoint.first + imageWidth, startPoint.second + imageHeight };

    const auto iWidth = endPoint.first - startPoint.first;
    const auto iHeight = endPoint.second - startPoint.second;

    cimg_library::CImg<uint8_t> resultedImage(iWidth, iHeight, 1, 3);

    for (auto widthPixel = 0u; widthPixel < iWidth; ++widthPixel)
        for (auto heightPixel = 0u; heightPixel < iHeight; ++heightPixel)
            for (auto channelId = 0u; channelId < 3; ++channelId)
                resultedImage(widthPixel, heightPixel, channelId) = image(startPoint.first + widthPixel, startPoint.second + heightPixel, channelId);

    return resultedImage;
}






