#pragma once

#include "BaseImage.h"
#include "../Logger/Logger.h"

#include <opencv2/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/highgui.hpp>

class OpenCVStoredImage : public BaseImage
{
	public:
		OpenCVStoredImage(const std::string&);

		[[nodiscard]] const cv::Mat& GetRawImageObject() const;
		void Save(const std::string& outputPath, const std::string& fileName, ImageType extension) override;

	protected:
		cv::Mat m_Image;
};

