#include "CImgStoredImage.h"

CImgStoredImage::CImgStoredImage(const std::string& inputImagePath)
	: BaseImage(inputImagePath), m_Image {}
{
	try
	{
		m_Image.load(inputImagePath.c_str());
	}
	catch (cimg_library::CImgException e)
	{
		throw std::invalid_argument(e.what());
	}
}

[[nodiscard]] const cimg_library::CImg<uint8_t>& CImgStoredImage::GetRawImageObject() const
{
	return m_Image;
}

void CImgStoredImage::Save(const std::string& outputPath, const std::string& fileName, ImageType extension)
{
	const auto imageSaveFullPath = std::string(outputPath + fileName + GetStringFromImageType(extension));

	Logger::GetInstance().Log("Image saved in " + imageSaveFullPath, Logger::LogLevel::INFO);
	m_Image.save(imageSaveFullPath.c_str());
}