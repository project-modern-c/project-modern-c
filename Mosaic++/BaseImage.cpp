#include "BaseImage.h"
#include <regex>

BaseImage::BaseImage(const std::string& imagePath)
{
	if (std::smatch matches; std::regex_search(imagePath, matches, std::regex { "(\\/|\\\\){0}(\\w+)(\\.png|\\.jpg|\\.jpeg|\\.bmp)", std::regex_constants::icase | std::regex_constants::ECMAScript }))
	{
		m_fileName = matches[2];
		m_fileExtension = matches[3];
	}
}

[[nodiscard]] const std::string& BaseImage::GetFileName() const noexcept
{
	return m_fileName;
}

[[nodiscard]] const std::string& BaseImage::GetFileExtension() const noexcept
{
	return m_fileExtension;
}