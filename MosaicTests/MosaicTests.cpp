#include "CppUnitTest.h"

#include <cstdint>

#include "../Mosaic++/Utilities.h"

#include "../Mosaic++/Utilities.cpp"
#include "../Mosaic++/ImageSelfImpl.cpp"
#include "../Mosaic++/ImageCImgImpl.cpp"
#include "../Mosaic++/ImageOpenCVImpl.cpp"
#include "../Mosaic++/CImgStoredImage.cpp"
#include "../Mosaic++/OpenCVStoredImage.cpp"
#include "../Mosaic++/BaseImage.cpp"
#include "../Mosaic++/ImagesContainer.cpp"
#include "../Mosaic++/ConsoleInput.cpp"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace MosaicTests
{
	TEST_CLASS(MosaicTests)
	{
		public:
		
			TEST_METHOD(TestMedianColorBlack)
			{
				/*
					Imaginea data ca parametru functiei este una neagra deci culoarea medie este 0
				*/

				const auto image = cimg_library::CImg <uint8_t> { "../../MosaicTests/Images/black.png" };
				const auto imageMedianColor = utilities::GetMedianColor(image);
				Assert::AreEqual(0, static_cast<int>(imageMedianColor));
			}

			TEST_METHOD(TestMedianColorWhite)
			{
				/*
					Imaginea data ca parametru functiei este una alba deci culoarea medie este 255
				*/

				const auto image = cimg_library::CImg <uint8_t>{ "../../MosaicTests/Images/white.png" };
				const auto imageMedianColor = utilities::GetMedianColor(image);
				Assert::AreEqual(255, static_cast<int>(imageMedianColor));
			}

			TEST_METHOD(TestMedianColorRedHue)
			{
				/*
					Imaginea data ca parametru functiei este una alba deci culoarea medie este 255
				*/

				const auto imageSelf = cimg_library::CImg <uint8_t>{ "../../MosaicTests/Images/red.png" };
				const auto imageMedianColorSelf = utilities::GetMedianColor(imageSelf);

				const auto imageCV = cv::imread("../../MosaicTests/Images/red.png", cv::IMREAD_UNCHANGED);
				const auto imageMedianColorCV = utilities::GetMedianColor(imageCV);

				Assert::AreEqual(static_cast<int>(imageMedianColorSelf), static_cast<int>(imageMedianColorCV));
			}

			TEST_METHOD(TestMedianColorGreenHue)
			{
				/*
					Imaginea data ca parametru functiei este una alba deci culoarea medie este 255
				*/

				const auto imageSelf = cimg_library::CImg <uint8_t>{ "../../MosaicTests/Images/green.jpeg" };
				const auto imageMedianColorSelf = utilities::GetMedianColor(imageSelf);

				const auto imageCV = cv::imread("../../MosaicTests/Images/green.jpeg", cv::IMREAD_UNCHANGED);
				const auto imageMedianColorCV = utilities::GetMedianColor(imageCV);

				Assert::AreEqual(static_cast<int>(imageMedianColorSelf), static_cast<int>(imageMedianColorCV));
			}

			TEST_METHOD(TestMedianColorPhoto)
			{
				/*
					Imaginea data ca parametru functiei este una alba deci culoarea medie este 255
				*/

				const auto imageSelf = cimg_library::CImg <uint8_t>{ "../../MosaicTests/Images/photo.jpg" };
				const auto imageMedianColorSelf = utilities::GetMedianColor(imageSelf);

				const auto imageCV = cv::imread("../../MosaicTests/Images/photo.jpg", cv::IMREAD_UNCHANGED);
				const auto imageMedianColorCV = utilities::GetMedianColor(imageCV);

				Assert::AreEqual(static_cast<int>(imageMedianColorSelf), static_cast<int>(imageMedianColorCV));
			}

			TEST_METHOD(TestInterpolationBiggerWhite)
			{
				auto image = cimg_library::CImg <uint8_t>{ "../../MosaicTests/Images/white.png" };
				image.display();

				const auto interpolatedImage = utilities::GetScaledImage(image, 400, 400);
				interpolatedImage.display();
			}
			
			TEST_METHOD(TestInterpolationSmallerWhite)
			{
				auto image = cimg_library::CImg <uint8_t>{ "../../MosaicTests/Images/white.png" };
				image.display();

				const auto interpolatedImage = utilities::GetScaledImage(image, 50, 50);
				interpolatedImage.display();
			}

			TEST_METHOD(TestInterpolationBiggerBlack)
			{
				auto image = cimg_library::CImg <uint8_t>{ "../../MosaicTests/Images/black.png" };
				image.display();

				const auto interpolatedImage = utilities::GetScaledImage(image, 600, 600);
				interpolatedImage.display();
			}
			
			TEST_METHOD(TestInterpolationSmallerBlack)
			{
				auto image = cimg_library::CImg <uint8_t>{ "../../MosaicTests/Images/black.png" };
				image.display();

				const auto interpolatedImage = utilities::GetScaledImage(image, 10, 10);
				interpolatedImage.display();
			}
			
			TEST_METHOD(TestInterpolationBiggerYellow)
			{
				auto image = cimg_library::CImg <uint8_t>{ "../../MosaicTests/Images/yellow.png" };
				image.display();

				const auto interpolatedImage = utilities::GetScaledImage(image, 1000, 1000);
				interpolatedImage.display();
			}
			
			TEST_METHOD(TestInterpolationSmallerYellow)
			{
				auto image = cimg_library::CImg <uint8_t>{ "../../MosaicTests/Images/yellow.png" };
				image.display();

				const auto interpolatedImage = utilities::GetScaledImage(image, 30, 30);
				interpolatedImage.display();
			}

			TEST_METHOD(TestInterpolationBiggerGreen)
			{
				auto image = cimg_library::CImg <uint8_t>{ "../../MosaicTests/Images/green.jpeg" };
				image.display();

				const auto interpolatedImage = utilities::GetScaledImage(image, 1000, 1000);
				interpolatedImage.display();
			}

			TEST_METHOD(TestInterpolationSmallerGreen)
			{
				auto image = cimg_library::CImg <uint8_t>{ "../../MosaicTests/Images/green.jpeg" };
				image.display();

				const auto interpolatedImage = utilities::GetScaledImage(image, 30, 30);
				interpolatedImage.display();
			}

			TEST_METHOD(TestInterpolationBiggerRed)
			{
				auto image = cimg_library::CImg <uint8_t>{ "../../MosaicTests/Images/red.png" };
				image.display();

				const auto interpolatedImage = utilities::GetScaledImage(image, 1000, 1000);
				interpolatedImage.display();
			}

			TEST_METHOD(TestInterpolationSmallerRed)
			{
				auto image = cimg_library::CImg <uint8_t>{ "../../MosaicTests/Images/red.png" };
				image.display();

				const auto interpolatedImage = utilities::GetScaledImage(image, 30, 30);
				interpolatedImage.display();
			}
		
			TEST_METHOD(TestInputGetFileName)
			{
				const auto image = ImageSelfImpl { "../../MosaicTests/Images/black.png" };

				Assert::AreEqual("black", image.GetFileName().c_str());
			}

			TEST_METHOD(TestInputGetFileExtensionPNG)
			{
				const auto image = ImageSelfImpl { "../../MosaicTests/Images/black.png" };

				Assert::AreEqual(".png", image.GetFileExtension().c_str());
			}
			TEST_METHOD(TestInputGetFileExtensionJPG)
			{
				const auto image = ImageSelfImpl { "../../MosaicTests/Images/black.jpg" };

				Assert::AreEqual(".jpg", image.GetFileExtension().c_str());
			}
			
			TEST_METHOD(TestInputGetFileExtensionJPEG)
			{
				const auto image = ImageSelfImpl { "../../MosaicTests/Images/black.jpeg" };

				Assert::AreEqual(".jpeg", image.GetFileExtension().c_str());
			}
			
			TEST_METHOD(TestInputGetFileExtensionBMP)
			{
				const auto image = ImageSelfImpl { "../../MosaicTests/Images/black.bmp" };

				Assert::AreEqual(".bmp", image.GetFileExtension().c_str());
			}
	};
}
