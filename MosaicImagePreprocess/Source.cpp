#include "CImg.h"

#include <fstream>
#include <filesystem>

#include "..\Mosaic++\Utilities.h"

inline void WriteImageToFile(std::ofstream& file, std::filesystem::directory_entry entry)
{
    std::string path_string{ entry.path().string() };

    cimg_library::CImg<uint8_t> image{ path_string.c_str() };

    /*
        Pentru ca toate imaginile sa fie de forma patratica
    */

    if (image.height() != image.width())
    {
        image = utilities::GetScaledImage(image, image.width(), image.width());
        image.save(path_string.c_str());
    }

    uint32_t medianColor = utilities::GetMedianColor(image);

    file << medianColor << " " << image.height() << " " << path_string << '\n';
};

inline void IterateEntry(std::ofstream& file, std::filesystem::directory_entry entry)
{
    if (entry.is_directory())
        for (const auto& newEntry : std::filesystem::directory_iterator(entry.path()))
            IterateEntry(file, newEntry);
    else
        WriteImageToFile(file, entry);
}

int main()
{
    std::ofstream file { "../Mosaic++/images.txt" };

    if (file.bad())
        throw std::exception("Images file has not opened correctly!");

    std::string path = "../Mosaic++/Images";

    for (const auto& entry : std::filesystem::directory_iterator(path))
        IterateEntry(file, entry);
    
    file.close();

    return 0;
}

