#pragma once

#include <cstdint>
#include <ostream>
#include <ctime>
#include <iomanip>
#include <string_view>
#include <memory>

#ifdef LOGGER_EXPORTS
#define API_LOGGER __declspec(dllexport)
#else
#define API_LOGGER __declspec(dllimport)
#endif

#undef ERROR

class API_LOGGER Logger final
{
public:

	enum class LogLevel : uint8_t
	{
		INFO,
		WARNING,
		ERROR
	};

private:
	Logger();

public:

	static Logger& GetInstance();

	Logger(Logger const&) = delete;
	Logger& operator = (Logger const&) = delete;

	void SetLogLevel(const LogLevel);
	void SetOutputStream(std::ostream& outputStream);

	void Log(const std::string_view, LogLevel) const;
	void Log(const char*, LogLevel) const;

private:

	std::reference_wrapper<std::ostream> m_outputStream;
	LogLevel m_minLevel;
};