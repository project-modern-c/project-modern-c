#include "Logger.h"
#include <iostream>


Logger::Logger()
	: m_outputStream(std::cout), m_minLevel(Logger::LogLevel::INFO)
{
}

Logger& Logger::GetInstance()
{
	static Logger logger;
	return logger;
}

void Logger::SetLogLevel(const LogLevel level)
{
	m_minLevel = level;
}

void Logger::SetOutputStream(std::ostream& outputStream)
{
	m_outputStream = outputStream;
}

const std::string_view LevelToString(Logger::LogLevel level)
{
	switch (level)
	{
	case Logger::LogLevel::INFO:
		return "INFO";
	case Logger::LogLevel::ERROR:
		return "ERROR";
	case Logger::LogLevel::WARNING:
		return "WARNING";
	default:
		throw std::invalid_argument("Invalid level");
	}
}

#pragma warning (disable:4996) //'safe'

void Logger::Log(const std::string_view message, LogLevel logLevel) const
{
	if (static_cast<uint8_t>(logLevel) < static_cast<uint8_t>(m_minLevel))
		return;

	const auto currentTime = std::time(nullptr);
	m_outputStream.get() << "[" << LevelToString(logLevel) << "] [" << std::put_time(std::localtime(&currentTime), "%Y-%m-%d %H:%M:%S") << "] " << message << '\n';
}

void Logger::Log(const char* message, LogLevel logLevel) const
{
	return this->Log(std::string_view{ message }, logLevel);
}
